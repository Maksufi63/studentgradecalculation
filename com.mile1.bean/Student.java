package com.mile1.bean;


public class Student {
	
	
	//constructors
	public Student() {
		super();
	}
	public Student(String name, int[] marks) {
		super();
		this.name = name;
		this.marks = marks;
	}
	
	//Instance Variables
	String name;
	public int marks[]=new int[3];
	
	//Getters and Setters
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int[] getMarks() {
		return marks;
	}
	public void setMarks(int[] marks) {
		this.marks = marks;
	}
	
	
	
}

