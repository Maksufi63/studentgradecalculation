package com.mile1.main;

import com.mile1.bean.Student;
import com.mile1.exception.NullMarksArrayException;
import com.mile1.exception.NullNameException;
import com.mile1.exception.NullStudentException;
import com.mile1.service.StudentReport;
import com.mile1.service.StudentService;

public class StudentMain {
	
	static Student data[]=new Student[4];
	
	static {
		for (int i = 0; i < data.length; i++) 	data [i]= new Student(); 	
        
		data [0] = new Student("Sufi", new int[]{100,90,90});
		data [1] = new Student("Fayaz", new int[]{11,22,33});
		data [2] = null;
		data [3] = new Student("Manoj", null);

	}
	
	public static void main(String args[]) throws NullStudentException, NullNameException, NullMarksArrayException
	{
		StudentReport srep=new StudentReport();
		StudentService sser=new StudentService(); 
		String x=null;
		for (int i = 0; i < data.length; i++) 
		{ 
			try {
				x=srep.validate(data[i]);
			}
			catch(NullNameException e)
			{
				x="NullNameException Occured";
			}
			
			catch(NullMarksArrayException e)
			{
				x="NullMarksArrayException Occured";
			}
			
			catch(NullStudentException e)
			{
				x="NullStudentException Occured";
			}
			System.out.println("GRADE= "+x);
		}
		System.out.println ("Number of Objects with Marks array as null = "+ sser.findNumberOfNullMarks (data));
		System.out.println ("Number of Objects with Name as null = "+ sser.findNumberOfNullNames(data));	
		System.out.println ("Number of Objects that are entirely null = "+ sser.findNumberOfNullObjects(data));

		
	}

}
