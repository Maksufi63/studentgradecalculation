package com.mile1.service;

import com.mile1.exception.NullMarksArrayException;
import com.mile1.exception.NullNameException;
import com.mile1.exception.NullStudentException;
import com.mile1.bean.Student;

public class StudentReport {
	
	public String findGrade(Student studentObject)
	{
		String Grade="";
		int sum=0;
		for(int i1=0;i1<studentObject.getMarks().length;i1++)
			sum+=studentObject.marks[i1];
		for(int i=0;i<studentObject.getMarks().length;i++)
		{
			if(studentObject.marks[i]<35)
			{
				Grade="F";
			}
			else
			{
				if(sum<=150)
					Grade="D";
				else if(sum>150 && sum<=200)
					Grade="C";
				else if(sum>200 && sum<=250)
					Grade="B";
				else if(sum>250 && sum<=300)
					Grade="A";
			}
		}
		return Grade;
	}
	//Validate Method2
		public String validate(Student studentObject) throws NullStudentException, NullNameException, NullMarksArrayException
		{
			if(studentObject == null)
				throw new NullStudentException();
			else
			{
				if(studentObject.getName()==null)
					throw new NullNameException();
				else if(studentObject.getMarks()==null)
					throw new NullMarksArrayException();
				else
				{
					return findGrade(studentObject);
				}
			}
		}

}
