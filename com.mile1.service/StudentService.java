package com.mile1.service;

import com.mile1.bean.Student;

public class StudentService {
	
	public int findNumberOfNullMarks(Student data[])
	{
		int marks_count=0;
		for(int i=0;i<data.length;i++)
		{
			if(data[i]!=null)
			{
				if(data[i].marks==null)
					marks_count++;
			}
		}
		return marks_count;
	}
	
	//
	
	public int findNumberOfNullNames(Student data[])
	{
		int names_count=0;
		for(int i=0;i<data.length;i++)
		{
			if(data[i]!=null)
			{
				if(data[i].getName()==null)
					names_count++;
			}
		}
		return names_count;
	}
	
	//
	
	public int findNumberOfNullObjects(Student data[])
	{
		int object_count=0;
		for(int i=0;i<data.length;i++)
		{
			if(data[i]==null)
				object_count++;
		}
		return object_count;
	}
	

}
